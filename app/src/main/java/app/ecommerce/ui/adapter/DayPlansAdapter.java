package app.ecommerce.ui.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.CitySights;

public class DayPlansAdapter extends RecyclerView.Adapter<DayPlansAdapter.ViewHolder> {

    private List<CitySights> list;
    private Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();

    public DayPlansAdapter(List<CitySights> list, Context context){
        this.list = list;
        this.context = context;
        for (int i = 0; i < list.size(); i++) {
            expandState.append(i, false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_plan_card_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView counter;
        private RecyclerView recycler_view_image, recycler_view_destination;
        private ImageView icon_drop;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.card_view_title);
            counter = itemView.findViewById(R.id.card_view_sights_count);
            icon_drop = itemView.findViewById(R.id.icon_drop);
            recycler_view_image = itemView.findViewById(R.id.recycler_view_image_sights);
            recycler_view_destination = itemView.findViewById(R.id.recycler_view_destination);
        }
        public void bind(CitySights sight){
            title.setText(sight.getTitle());
            counter.setText(sight.getCounter());
            recycler_view_image.setAdapter(new ImageAdapter(sight.getImages()));
            recycler_view_destination.setLayoutManager(new LinearLayoutManager(context));

//            expandable
            boolean isExpanded = expandState.get(getAdapterPosition());
            recycler_view_destination.setVisibility(isExpanded?View.VISIBLE:View.GONE);
            recycler_view_destination.setAdapter(new DestinationAdapter(sight.getDestinations(), context));

            icon_drop.setRotation(expandState.get(getAdapterPosition()) ? 180f : 0f);
            icon_drop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickButton(recycler_view_destination, icon_drop, getAdapterPosition());
                }
            });
        }
        private void onClickButton(final RecyclerView expandableLayout, final ImageView buttonLayout, final  int i) {

            if (expandableLayout.getVisibility() == View.VISIBLE){
                createRotateAnimator(buttonLayout, 180f, 0f).start();
                expandableLayout.setVisibility(View.GONE);
                expandState.put(i, false);
            }else{
                createRotateAnimator(buttonLayout, 0f, 180f).start();
                expandableLayout.setVisibility(View.VISIBLE);
                expandState.put(i, true);
            }
        }

        //Code to rotate button
        private ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
            animator.setDuration(300);
            animator.setInterpolator(new LinearInterpolator());
            return animator;
        }
    }
}
