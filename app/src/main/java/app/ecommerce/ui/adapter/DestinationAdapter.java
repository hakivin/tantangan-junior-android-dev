package app.ecommerce.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.Destination;

public class DestinationAdapter extends RecyclerView.Adapter<DestinationAdapter.ViewHolder> {
    private List<Destination> destinations;
    private Context context;

    public DestinationAdapter(List<Destination> destinations, Context context){
        this.destinations = destinations;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_sight_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(destinations.get(position));
    }

    @Override
    public int getItemCount() {
        return destinations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView walk_image;
        private TextView circle_number, distance_text, destination_text, open_hour_text, visitor_wait_time_text;
        private CardView card_view;
        private View end_step;
        public ViewHolder(@NonNull View v) {
            super(v);
            walk_image = v.findViewById(R.id.walk_image);
            circle_number = v.findViewById(R.id.circle_number);
            distance_text = v.findViewById(R.id.distance_text);
            destination_text = v.findViewById(R.id.destination_text);
            open_hour_text = v.findViewById(R.id.open_hour_text);
            visitor_wait_time_text = v.findViewById(R.id.visitor_wait_time_text);
            card_view = v.findViewById(R.id.card_view_destination);
            end_step = v.findViewById(R.id.end_step);
        }
        public void bind(Destination destination){
            if (getAdapterPosition()%2 == 0)
                card_view.setBackgroundColor(context.getResources().getColor(R.color.grey_50));
            else {
                destination_text.setTextSize(16);
            }
            if (destination.getDistance().isEmpty()){
                walk_image.setVisibility(View.GONE);
                distance_text.setVisibility(View.GONE);
            } else {
                distance_text.setText(destination.getDistance());
            }
            if (destination.getOpenHour().isEmpty()){
                open_hour_text.setVisibility(View.GONE);
            } else {
                open_hour_text.setText(destination.getOpenHour());
            }
            String pos = String.valueOf((getAdapterPosition()+1));
            circle_number.setText(pos);
            destination_text.setText(destination.getName());
            visitor_wait_time_text.setText(destination.getVisitorWait());
            if (getAdapterPosition() == getItemCount()-1)
                end_step.setVisibility(View.VISIBLE);
        }
    }
}
