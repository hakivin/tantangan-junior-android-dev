package app.ecommerce.ui.model;

import java.util.List;

public class CitySights {
    private int[] images;
    private String title;
    private String counter;
    private List<Destination> destinations;

    public CitySights(int[] images, String title, String counter, List<Destination> destinations) {
        this.images = images;
        this.title = title;
        this.counter = counter;
        this.destinations = destinations;
    }

    public CitySights(){

    }

    public int[] getImages() {
        return images;
    }

    public void setImages(int[] images) {
        this.images = images;
        this.counter = images.length + " sights";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }
}
