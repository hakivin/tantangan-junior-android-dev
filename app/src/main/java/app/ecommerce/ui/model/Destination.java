package app.ecommerce.ui.model;

public class Destination {
    private String distance, name, openHour, visitorWait;

    public Destination(String distance, String name, String openHour, String visitorWait) {
        this.distance = distance;
        this.name = name;
        this.openHour = openHour;
        this.visitorWait = visitorWait;
    }

    public Destination() {

    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getVisitorWait() {
        return visitorWait;
    }

    public void setVisitorWait(String visitorWait) {
        this.visitorWait = visitorWait;
    }
}
