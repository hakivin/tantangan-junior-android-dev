package app.ecommerce.ui.data;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.CitySights;
import app.ecommerce.ui.model.Destination;
import app.ecommerce.ui.model.ExampleProduct;

@SuppressWarnings("ResourceType")
public class DataGenerator {

    /**
     * Generate dummy data shopping product
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<ExampleProduct> getProducts(Context ctx) {
        List<ExampleProduct> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.product_image);
        String title_arr[] = ctx.getResources().getStringArray(R.array.product_title);
        String price_arr[] = ctx.getResources().getStringArray(R.array.product_price);
        for (int i = 0; i < drw_arr.length(); i++) {
            ExampleProduct obj = new ExampleProduct();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.title = title_arr[i];
            obj.price = price_arr[i];
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }

    public static List<CitySights> getCitySights(Context context){
        List<CitySights> sights = new ArrayList<>();
        TypedArray sights_arr = context.getResources().obtainTypedArray(R.array.sights);
        String title_arr[] = context.getResources().getStringArray(R.array.day_plans_title);
        List<List<Destination>> destinations = getDestinations(context);
        int a = sights_arr.length();
        for (int i = 0; i < a; i++){
            CitySights sight = new CitySights();
            sight.setTitle(title_arr[i]);
            sight.setDestinations(destinations.get(i));
            int id = sights_arr.getResourceId(i, -1);
            TypedArray drw_arr = context.getResources().obtainTypedArray(id);
            int[] drawables = new int[drw_arr.length()];
            for (int x = 0; x < drawables.length; x++){
                drawables[x] = drw_arr.getResourceId(x,-1);
            }
            if (id > -1)
                sight.setImages(drawables);
            sights.add(sight);
        }
        sights_arr.recycle();
        return sights;
    }

    private static List<List<Destination>> getDestinations(Context context){
        List<List<Destination>> destinations = new ArrayList<>();
        TypedArray sights_arr = context.getResources().obtainTypedArray(R.array.sights);
        for (int i = 0; i < sights_arr.length(); i++) {
            destinations.add(new ArrayList<Destination>());
        }
        String[] distance = context.getResources().getStringArray(R.array.distance);
        String[] destination = context.getResources().getStringArray(R.array.destination);
        String[] openHour = context.getResources().getStringArray(R.array.open_hour);
        String[] visitorWait = context.getResources().getStringArray(R.array.visitor_wait);
        for (int a = 0; a < distance.length; a++){
            Destination obj = new Destination();
            obj.setDistance(distance[a]);
            obj.setName(destination[a]);
            obj.setOpenHour(openHour[a]);
            obj.setVisitorWait(visitorWait[a]);
            destinations.get(1).add(obj);
        }
        return destinations;
    }
}
