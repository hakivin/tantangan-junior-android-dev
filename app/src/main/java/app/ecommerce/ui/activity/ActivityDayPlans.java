package app.ecommerce.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.DayPlansAdapter;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.model.CitySights;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity {

    private RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);
        initToolbar();
        initComponent();
    }

    private void initToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar_plans);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.deep_purple_700);
    }

    private void initComponent(){
        recycler_view = findViewById(R.id.recycler_view_day_plans);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(false);

        List<CitySights> sightsList = DataGenerator.getCitySights(this);
        recycler_view.setAdapter(new DayPlansAdapter(sightsList, this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.day_plans_menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
